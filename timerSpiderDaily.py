import subprocess
from scrapy import cmdline
from apscheduler.schedulers.background import BlockingScheduler
import datetime

def SpiderRun():
    print(f'----------------------开始执行爬虫 时间：{datetime.datetime.now()}---------------------------------')
    subprocess.run('scrapy crawl national') #执行代码

if __name__ == '__main__':

    sche = BlockingScheduler()
    sche.add_job(SpiderRun,'interval',hours=1)

    try:
        sche.start()
    except:
        print('出现了错误！！')
