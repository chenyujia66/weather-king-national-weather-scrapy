import MySQLdb
from pyecharts.charts import Bar,Pie
from pyecharts import options as opt

def database_data():
    #连接数据库
    conn = MySQLdb.connect(db='tianqiwang',
                           host='localhost',
                           user='root',
                           password='123456',
                           charset='utf8')
    print('数据库连接成功！！！')
    cursor = conn.cursor() #获得游标
    sql = 'select 风向,总 from guangdong_direction' #sql查询语句
    cursor.execute(sql)
    rows = cursor.fetchall() #获取所有数据
    wind_directions = []
    nums = []
    for row in rows: #遍历数据
        wind_direction = row[0] #获取风向
        num = row[1] #获取总数
        wind_directions.append(wind_direction) #将风向类型加入到风向列表中
        nums.append(num) #将各风向类型的数量和放到列表中
    print(wind_directions,nums)
    cursor.close() #关闭游标
    conn.close() #断开数据库连接

    # #声明一个Bar对象
    # bar = Bar()
    # #添加标题
    # bar.set_global_opts(
    #     title_opts = opt.TitleOpts(title='柱状图',subtitle='广东省内各风向级别数量')
    # )
    # #x轴显示风向和级别
    # bar.add_xaxis(wind_directions)
    # #y轴显示风向级别的数量
    # bar.add_yaxis('风向级别数量',nums)
    # bar.render('广东省内各风向级别数量.html')

    pie = Pie()
    pie.add('风向级别数量', [list(i) for i in zip(wind_directions, nums)])  # 显示数据设置
    pie.set_global_opts(
        title_opts=opt.TitleOpts(title='饼状图', subtitle='广东省内风向级别数量')
    )
    pie.set_series_opts(
        label_opts=opt.LabelOpts(formatter="{b}: {d}%")  # 显示百分比
    )
    pie.render('广东省内各风向级别数量-饼状图.html')

if __name__ == '__main__':
    database_data()
