# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

import time
class TianqiwangPipeline:
    def process_item(self, item, spider):
        return item
import os
class CSVPipeline(object):
    #将数据按照规定写入CSV的类
    file = None
    new_time = time.strftime('%Y-%m-%d')
    #爬虫打开时，执行
    def open_spider(self,spider):
        file = f'national_weather_{self.new_time}.csv'
        if os.path.exists(file):
            self.index = 1
        else:
            self.index = 0
        self.file = open(file,'a',encoding='utf-8') #打开national_weather.csv文件
    #写入数据的函数
    def process_item(self, item, spider):
        if self.index == 0:
            column_name = '地区,时间,温度,天气,今日温度范围,昨天温度范围,风向,湿度,紫外线,气压\n'
            self.file.write(column_name)
            self.index = 1
        all_info = item['title']+','+item['now_time']+','+item['now_temperature']+','+item['now_weather']+','+item['today_temperature']+','+item['yesterday_temperature']+','+item['wind_direction']+','+item['humidity']+','+item['radial']+','+item['pressure']+'\n'
        self.file.write(all_info)
        return item #写入后返回
    #爬虫结束时，执行
    def close_spider(self,spider):
        self.file.close()
import MySQLdb
class MySQLPipeline(object):
    def open_spider(self,spider):
        db_name = spider.settings.get('MYSQL_DB_NAME','weather_climate')
        host = spider.settings.get('MYSQL_HOST','localhost')
        user = spider.settings.get('MYSQL_USER','root')
        password = spider.settings.get('MYSQL_PASSWORD','123456')
        self.db_conn = MySQLdb.connect(db=db_name,
                                       host=host,
                                       user=user,
                                       password=password,
                                       charset='utf8')
        self.db_cursor = self.db_conn.cursor()

    def process_item(self, item, spider):
        values = (item['title'],item['now_time'],item['now_temperature'],item['now_weather'],item['today_temperature'],item['yesterday_temperature'],item['wind_direction'],item['humidity'],item['radial'],item['pressure'])
        sql = 'insert into real_time_national_weather(地区,时间,温度,天气,今日温度范围,昨天温度范围,风向,湿度,紫外线,气压) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
        self.db_cursor.execute(sql,values)
        return item
    def close_spider(self,spider):
        self.db_conn.commit()
        self.db_cursor.close()
        self.db_conn.close()
