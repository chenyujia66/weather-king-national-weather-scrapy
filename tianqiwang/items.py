# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class TianqiwangItem(scrapy.Item):
    # define the fields for your item here like:
    title = scrapy.Field() #地方标题
    now_time = scrapy.Field() #获取当前的时间
    now_temperature = scrapy.Field() #获取当前时刻的温度
    now_weather = scrapy.Field() #获取当前时刻天气
    today_temperature = scrapy.Field() #今天的温度
    yesterday_temperature = scrapy.Field() #昨天的温度
    wind_direction = scrapy.Field() #风向
    humidity = scrapy.Field() #湿度
    radial = scrapy.Field() #紫外线
    pressure = scrapy.Field() #气压

