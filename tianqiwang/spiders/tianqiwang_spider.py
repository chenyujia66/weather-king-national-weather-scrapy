#coding:utf-8
import copy

#未运用scrapy-redis
from scrapy import Request
from scrapy.spiders import Spider
from tianqiwang.items import TianqiwangItem
#
import time
# #运用scrapy-redis
# import scrapy
# from scrapy import Item,Field
# from scrapy_redis.spiders import RedisSpider

class TianqiwangSpider(Spider):
    name = 'national'

    def start_requests(self):
        url = 'https://tianqi.2345.com/china.htm'
        yield Request(url)


    def parse(self, response):
        list_selector = response.xpath('//div[@class="box-mod-tb"]/ul/li/a/@href').extract()
        for one_selector in list_selector:
            try:
                url = response.urljoin(one_selector)
                # print(url)
                yield Request(url,callback=self.position_parse)
            except:
                pass

    def position_parse(self,response):
            item = TianqiwangItem()
            list_selector = response.xpath('//table[@class="provincial-city-table"]/tr/td[2]/a')
            for one_selector in list_selector:
                try:
                    #获取地方标题
                    title = one_selector.xpath('.//text()').extract()[0]
                    # print(title)
                    #获取相对url
                    href = one_selector.xpath('.//@href').extract()[0]
                    #获取绝对url
                    url = response.urljoin(href)
                    item['title'] = title
                    # print(item['title'])
                    yield Request(url,meta={'item':copy.deepcopy(item)},callback=self.deta_parse)
                except:
                    pass

    def deta_parse(self,response):
        try:
            #获取当前的时间
            now_time = time.strftime('%Y-%m-%d %H:%M:%S')
            # print(now_time)
            #获取实时温度
            now_temperature = response.xpath('//span[@class="real-t"]/text()').extract()[0]
            # print(now_temperature)
            #获取实时天气
            now_weather = response.xpath('//em[@class="cludy"]/text()').extract()[0]
            # print(now_weather)
            #获取今天预计的温度
            today_temperature = response.xpath('//div[@class="real-today"]/span/text()').extract()[0].replace('今天：','').replace(' ','-')
            # print(today_temperature)
            #获取昨天的温度
            yesterday_temperature = response.xpath('//p[@class="other-info"]/span/text()').extract()[0].replace('昨天：','').replace(' ','-')
            # print(yesterday_temperature)
            #获取风向
            wind_direction = response.xpath('//ul[@class="real-data"]/li/span/text()').extract()[0].replace('风向','').replace('\xa0',' ')
            # print(wind_direction)
            #获取湿度
            humidity = response.xpath('//ul[@class="real-data"]/li/span/text()').extract()[1].replace('湿度','').replace('\xa0',' ')
            # print(humidity)
            #获取紫外线
            radial = response.xpath('//ul[@class="real-data"]/li/span/text()').extract()[2].replace('紫外线','').replace('\xa0',' ')
            # print(radial)
            #获取气压
            pressure = response.xpath('//ul[@class="real-data"]/li/span/text()').extract()[3].replace('\xa0',' ').replace('气压 ','')
            # print(pressure)
            item = response.meta['item']
            item['now_time'] = now_time
            item['now_temperature'] = now_temperature
            item['now_weather'] = now_weather
            item['today_temperature'] = today_temperature
            item['yesterday_temperature'] = yesterday_temperature
            item['wind_direction'] = wind_direction
            item['humidity'] = humidity
            item['radial'] = radial
            item['pressure'] = pressure
            yield item

        except:
            pass

