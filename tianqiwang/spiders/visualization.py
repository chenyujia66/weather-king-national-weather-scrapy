#coding:utf-8

from pyecharts.charts import Bar,Pie
from pyecharts import options as opt
from tianqiwang.database_data_1 import database_data

wind_directions,nums = database_data() #从数据库中获取到数据

# #声明一个Bar对象
# bar = Bar()
# #添加标题
# bar.set_global_opts(
#     title_opts = opt.TitleOpts(title='柱状图',subtitle='全国各风向级别数量')
# )
# #设置x轴数据
# bar.add_xaxis(wind_directions)
# #设置y轴数据
# bar.add_yaxis('风向级别数量',nums)
# #生成html文件
# bar.render('全国各风向级别数量1021.html')
#
# pie = Pie()
# pie.add('风向级别数量',[list(i) for i in zip(wind_directions,nums)]) #显示数据设置
# pie.set_global_opts(
#     title_opts = opt.TitleOpts(title='饼状图',subtitle='全国各风向级别数量')
# )
# pie.set_series_opts(
#     label_opts = opt.LabelOpts(formatter="{b}: {d}%") #显示百分比
# )
# pie.render('全国各风向级别数量-饼状图1021.html')

print(wind_directions,nums)