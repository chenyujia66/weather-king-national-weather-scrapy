import MySQLdb

def database_data():
    #连接数据库
    conn = MySQLdb.connect(db='weather_climate',
                           host='localhost',
                           user='root',
                           password='123456',
                           charset='utf8')
    cursor = conn.cursor() #获得游标
    sql = 'select 风向,count(*) from national group by 风向' #sql查询语句
    cursor.execute(sql) #执行sql语句
    rows = cursor.fetchall() #获取所有数据
    wind_directions = []
    nums = []
    for row in rows:
        wind_direction = row[0]
        num = row[1]
        wind_directions.append(wind_direction)
        nums.append(num)
    cursor.close()
    conn.close()
    print(wind_directions,nums)
    return wind_directions,nums


if __name__ == '__main__':
    database_data()